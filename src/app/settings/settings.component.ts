import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  users?: string[];
  modelUserId?: number;
  currentUser?: string;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.users = this.userService.getAll()
    this.userService.getUser().subscribe(user => this.currentUser = user);
    this.modelUserId = this.users.findIndex(user => user == this.currentUser);
  }

  changeUser(id: number) {
    this.userService.setUser(id)
  }
}
