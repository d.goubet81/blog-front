import { Component, OnInit } from '@angular/core';
import { Post } from '../entities';
import { PostService } from '../service/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  lstPosts?: Post[];

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.postService.showAll().subscribe(list => this.lstPosts = list);
  }

  deletePost(post: Post) {
    if (post.id) {
      this.postService.delete(post.id).subscribe(
        () => {
          this.lstPosts?.splice(this.lstPosts.findIndex(item => item == post), 1)
        }
      );
    }
  }
}
