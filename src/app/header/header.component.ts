import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Category } from '../entities';
import { CategoryService } from '../service/category.service';
import { filter } from 'rxjs/operators';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  activeLinkClass: string = "fw-bold text-decoration-underline";

  lstCategories?: Category[];
  modelCatId: number = 0;

  currentUser?: string;

  constructor(
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private catService: CategoryService,
    private userService: UserService
  ) { }
  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.router.events.pipe(
      filter((event): event is NavigationEnd => event instanceof NavigationEnd))
      .subscribe(event => {
        const splitedUrl: string[] = event.url.split("/");
        if (splitedUrl[2] == "category") {
          this.modelCatId = Number(splitedUrl[3]);
        } else {
          this.modelCatId = 0;
        }
        this.cdRef.detectChanges();
      });

    this.catService.showAll().subscribe(list => this.lstCategories = list);
    this.userService.getUser().subscribe(user => this.currentUser = user);
  }

  displayByCategory() {
    if (this.modelCatId > 0) {
      this.router.navigate(['/post/category/', this.modelCatId]);
    } else {
      this.router.navigate(['/']);
    }
  }

  set(id: number) {
    this.userService.setUser(id);
  }
}
