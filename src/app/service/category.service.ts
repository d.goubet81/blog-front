import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private url: string;
  
  constructor(private http: HttpClient) {
    this.url = `${environment.settings.gateway}/api/category`;
  }

  public showAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.url);
  }

  public showById(id: number): Observable<Category> {
    return this.http.get<Category>(`${this.url}/${id}`);
  }

  public add(category: Category): Observable<Category> {
    return this.http.post<Category>(this.url, category);
  }
  
  public edit(category: Category): Observable<Category> {
    return this.http.put<Category>(`${this.url}/${category.id}`,category);
  }

  public delete(id:number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
