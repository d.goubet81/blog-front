import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private lstUsers: string[] = ['user', 'admin'];
  private user = new BehaviorSubject(this.lstUsers[0]);

  constructor() { }

  public getAll(): string[] {
    return this.lstUsers;
  }

  public getUser(): Observable<string> {
    return this.user;
  }

  public setUser(id: number) {
    if (id >= 0 && id <= this.lstUsers.length) {
      this.user.next(this.lstUsers[id]);
    }
  }
}