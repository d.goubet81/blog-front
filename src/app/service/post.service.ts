import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Post, Search } from '../entities';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.settings.gateway}/api/post`;
  }

  public showAll(): Observable<Post[]> {
    return this.http.get<Post[]>(this.url);
  }

  public showByCategory(id: number): Observable<Post[]> {
    const params = new HttpParams()
      .set('catId', id);
    return this.http.get<Post[]>(this.url, { params });
  }

  public showById(id: number): Observable<Post> {
    return this.http.get<Post>(`${this.url}/${id}`);
  }

  public add(post: Post): Observable<Post> {
    return this.http.post<Post>(this.url, post);
  }

  public edit(post: Post): Observable<Post> {
    return this.http.put<Post>(`${this.url}/${post.id}`, post);
  }

  public delete(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }

  public search(search: Search): Observable<Post[]> {
    let paramsMap = new Map<any, any>();
    paramsMap.set('title', search.inTitle);
    paramsMap.set('text', search.inText);
    if(search.category?.id){
      paramsMap.set('catId', search.category.id);
    }
    if(search.startDate){
      paramsMap.set('startDate', search.startDate);
    }
    if(search.endDate){
      paramsMap.set('endDate', search.endDate);
    }

    let params = new HttpParams();
    paramsMap.forEach((value: any, key: any) => {
      params = params.set(key, value);
    });
    return this.http.get<Post[]>(this.url, { params });
  }
}
