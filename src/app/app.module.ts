import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PostCardComponent } from './post/post-card/post-card.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { PostFormComponent } from './post/post-form/post-form.component';
import { SettingsComponent } from './settings/settings.component';
import { PostComponent } from './post/post/post.component';
import { PostListComponent } from './post/post-list/post-list.component';
import { FormsModule } from '@angular/forms';
import { PostByCategoryComponent } from './post/post-by-category/post-by-category.component';
import { PostNewComponent } from './post/post-new/post-new.component';
import { PostEditComponent } from './post/post-edit/post-edit.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SearchFormComponent } from './search/search-form/search-form.component';
import { SearchComponent } from './search/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    PostCardComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    PostFormComponent,
    SettingsComponent,
    PostComponent,
    PostListComponent,
    PostByCategoryComponent,
    PostNewComponent,
    PostEditComponent,
    NotFoundComponent,
    SearchFormComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
