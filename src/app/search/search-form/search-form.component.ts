import { formatDate } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category, Search } from 'src/app/entities';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  @Output()
  search: EventEmitter<Search> = new EventEmitter();

  modelSearch: Search = {
    inTitle: '',
    inText: '',
    endDate: formatDate(new Date(), 'yyyy-MM-dd', 'en')
  };

  maxDate: string = formatDate(new Date(), 'yyyy-MM-dd', 'en');

  modelLabel: string = '';
  modelSelectTitle: boolean = false;
  modelSelectText: boolean = true;

  lstCategories: Category[] = [];

  validation: boolean = false;

  constructor(
    private catService: CategoryService
  ) { }

  ngOnInit(): void {
    this.catService.showAll().subscribe(list => this.lstCategories = list);
  }

  onSubmitSearch(searchForm: NgForm) {
    this.setSearchInTitle();
    this.setSearchInText();
    this.search.emit(this.modelSearch)
  }

  setSearchInTitle() {
    if (this.modelSelectTitle) {
      this.modelSearch.inTitle = this.modelLabel;
    } else {
      this.modelSearch.inTitle = '';
    }
  }

  setSearchInText() {
    if (this.modelSelectText) {
      this.modelSearch.inText = this.modelLabel;
    } else {
      this.modelSearch.inText = '';
    }
  }

  checkEndDate() {
    if (this.modelSearch.startDate) {
      if (this.modelSearch.endDate < this.modelSearch.startDate) {
        this.modelSearch.endDate = this.modelSearch.startDate;
      }
    }
  }

  checkStartDate() {
    if (this.modelSearch.startDate) {
      if (this.modelSearch.startDate > this.modelSearch.endDate) {
        this.modelSearch.startDate = this.modelSearch.endDate;
      }
    }
  }
}
