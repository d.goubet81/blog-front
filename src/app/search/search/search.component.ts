import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Post, Search } from 'src/app/entities';
import { PostService } from 'src/app/service/post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  showResult: boolean = false;
  lstPosts: Post[] = [];
  results: number = 0;

  constructor(
    private location: Location,
    private router: Router,
    private postService: PostService) { }

  ngOnInit(): void {
    this.router.navigate([], {});
  }

  searchPosts(search: Search) {
    this.showResult = true;
    this.changeUrl(search);

    this.postService.search(search).subscribe(list => {
      this.lstPosts = list;
      this.results = this.lstPosts?.length;
    });
  }

  changeUrl(params:Search){
    const url = this.router.createUrlTree([], {
      queryParams: {
        'title': params.inTitle,
        'text': params.inText,
        'catId': params.category?.label,
        'startDate':params.startDate,
        'endDate':params.endDate
      }
    });
    this.location.go(url.toString());
  }

  deletePost(post: Post) {
    if (post.id) {
      this.postService.delete(post.id).subscribe(
        () => {
          this.lstPosts?.splice(this.lstPosts.findIndex(item => item == post), 1)
        }
      );
    }
  }
}
