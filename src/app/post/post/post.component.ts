import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { Post } from 'src/app/entities';
import { PostService } from 'src/app/service/post.service';
import { UserService } from 'src/app/service/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  post?: Post;
  user?: string;

  routeId?: number;

  defaultImg: string = environment.settings.defaultImgUrl;
  imgUrl: string = this.defaultImg;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private postService: PostService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.postService.showById(params['id']))
    ).subscribe(data => {
      this.post = data;
      if (this.post?.image) {
        this.imgUrl = `${environment.settings.imgUrl}/${this.post.image}`
      }
    });
    this.userService.getUser().subscribe(data => this.user = data);
  }

  deletePost() {
    if (this.post?.id) {
      this.postService.delete(this.post.id).subscribe(
        () => this.router.navigate(['/home']));
    }
  }
}
