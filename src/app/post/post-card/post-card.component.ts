import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { environment } from 'src/environments/environment';
import { Post } from '../../entities';

@Component({
  selector: 'app-post-card[post]',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit, OnChanges {
  user?: string;

  @Input()
  post?: Post;

  @Output()
  delPost: EventEmitter<Post> = new EventEmitter();

  defaultImg: string = environment.settings.defaultImgUrl;
  imgUrl: string = this.defaultImg;

  constructor(
    private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe(data => this.user = data);
  }

  ngOnChanges(): void {
    if (this.post?.image) {
      this.imgUrl = `${environment.settings.imgUrl}/${this.post.image}`;
    }
  }

  displayByCategory(id: number | undefined) {
    if (id) {
      this.router.navigate(['/post/category/', id]);
    } else {
      this.router.navigate(['/']);
    }
  }

  deletePost() {
    this.delPost.emit(this.post)
  }
}
