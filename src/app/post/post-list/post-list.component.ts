import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Post } from 'src/app/entities';

@Component({
  selector: 'app-post-list[lstPosts]',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  @Input()
  lstPosts?: Post[];

  @Output()
  delPost: EventEmitter<Post> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deletePost(post: Post) {
    this.delPost.emit(post);
  }

}
