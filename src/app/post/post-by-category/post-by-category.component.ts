import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { Post } from 'src/app/entities';
import { PostService } from 'src/app/service/post.service';

@Component({
  selector: 'app-post-by-category',
  templateUrl: './post-by-category.component.html',
  styleUrls: ['./post-by-category.component.css']
})
export class PostByCategoryComponent implements OnInit {

  lstPosts?: Post[];
  routeId?: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostService) { }

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.route.params.pipe(
      switchMap(params => this.postService.showByCategory(params['id']))
    ).subscribe(list => this.lstPosts = list);
  }

  deletePost(post: Post) {
    if (post.id) {
      this.postService.delete(post.id).subscribe(
        () => {
          this.lstPosts?.splice(this.lstPosts.findIndex(item => item == post), 1)
        }
      );
    }
  }
}
