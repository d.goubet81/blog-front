import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Post } from '../../entities';
import { PostService } from '../../service/post.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {

  post: Post = { title: "", text: "", date: new Date(), category: { label: "" }, image: "" };
  postTitle: string = "";

  constructor(
    private route: ActivatedRoute,
    private postService: PostService
  ) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.postService.showById(params['id']))
    ).subscribe(data => {
      this.post = data;
      this.postTitle = this.post.title;
    });
  }
}
