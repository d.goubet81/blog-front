import { Component, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Category,Post } from '../../entities';
import { CategoryService } from '../../service/category.service';
import { PostService } from '../../service/post.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Input()
  @Output()
  modelPost: Post = { title: "", text: "", date: new Date(), category: { label: "" }, image: "" };

  @Input()
  postEdit: boolean = false;

  createdPost?: Post;
  validation: boolean = false;

  lstCategories?: Category[];

  constructor(
    private router: Router,
    private location: Location,
    private postService: PostService,
    private catService: CategoryService) { }

  ngOnInit(): void {
    this.catService.showAll().subscribe(list => this.lstCategories = list);
  }

  onSubmitAddPost(postForm: NgForm) {
    this.validation = true;
    if (postForm.valid) {
      if (this.postEdit) {
        this.postService.edit(this.modelPost).subscribe({
          next: (post) => this.createdPost = post,
          error: (e) => console.log(e),
          complete: () => this.router.navigate(['/post/view', this.createdPost?.id])
        })
      } else {
        this.postService.add(this.modelPost).subscribe({
          next: (post) => this.createdPost = post,
          error: (e) => console.log(e),
          complete: () => this.router.navigate(['/post/view', this.createdPost?.id])
        })
      }
    }
  }

  cancel() {
    if (this.postEdit) {
      this.location.back();
    } else {
      this.router.navigate(['/home'])
    }
  }

}
