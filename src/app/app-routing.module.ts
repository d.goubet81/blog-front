import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { PostComponent } from './post/post/post.component';
import { PostByCategoryComponent } from './post/post-by-category/post-by-category.component';
import { PostNewComponent } from './post/post-new/post-new.component';
import { PostEditComponent } from './post/post-edit/post-edit.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SearchComponent } from './search/search/search.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'post/new', component: PostNewComponent },
  { path: 'post/edit/:id', component: PostEditComponent },
  { path: 'post/view/:id', component: PostComponent },
  { path: 'post/search', component: SearchComponent },
  { path: 'post/category/:id', component: PostByCategoryComponent },
  { path: 'settings', component: SettingsComponent },
  { path: '**', component:NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }