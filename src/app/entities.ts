export interface Category {
  id?: number;
  label: string;
}

export interface Post {
  id?: number;
  title: string;
  text: string;
  date: Date;
  category: Category;
  image: string;
}

export interface Search {
  inTitle: string;
  inText: string;
  startDate?: string;
  endDate: string;
  category?: Category;
}
