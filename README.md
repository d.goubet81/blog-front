# Projet Blog

L'objectif du projet est de créer une petite application de blog sans authentification en utilisant Spring Boot pour le backend et Angular pour le frontend.

[Lien vers le dépot backend](https://gitlab.com/d.goubet81/blog-back)

## Présentation
* Introduction
* Backend
  * Structure
  * Amélioration
* Frontend
  * Structure
  * Amélioration

### Introduction
***

L'idée initiale a été de créer des articles avec une catégorie et en fonction de l'avancement de rajouter la gestion des commentaires et des auteurs/utilisateurs.

Pour cela, nous avons une entité Post pour les articles et une entité Category pour les catégorie que l'on retrouve dans la base de données mySQL, dans la partie backend ainsi que dans la partie front end.

[Maquette de l'application](https://p18-dg-blog.tiiny.site/)

Il est possible de :
* Créer des articles
* Consulter la liste des articles
* Consulter un article spécifique
* Modifier ou supprimer un article
* Filtrer par catégorie
* Faire une recherche parmi les articles

Pour les images des articles, elles sont stockées au niveau du front et leur nom est mémorisé dans la base SQL. Une image par défaut est chargée si elle n'existe pas. 

### Backend
***

L'application Spring Boot regroupe toutes les fonctionnalités du CRUD.

#### Strucure

* Entity: Classes Post et Category avec getter/setter et contructeurs
* Repository: Repository pour faire les requêtes vers la base de données
* Controller: Controleurs pour réceptionner les requêtes HTTP

#### Amélioration

* Dans le PostController, la methode all qui permet d'aller chercher une liste d'article appelle une des 3 méthodes du repository en fonction des paramètres. La méthode search aurait pu être utilisé à chaque fois.

### Frontend
***

#### Structure

La page est constiué de :

* un header
* contenu de la page qui change en fonction de la navigation
  * Home avec l'affichage de tout les articles sous forme de card
  * Une vue de détail de l'article
  * Une vue des articles d'une même catégorie
  * Une page de création/modification
  * Une parge de recherche d'article
  * Une page de réglages pour sélectionner le type d'utilisateur (seul l'admin peur supprimer un article)
* un footer

Il est possible d'étiter/supprimer un article depuis la card ou depuis la vue de détails.

#### Amélioration

* Pour l'affichage des cards articles, afficher seulement le début du texte sur quelques lignes.
* Pour l'affichage de l'article solo, faire une mise en forme des paragraphes plus visuelle.
* Ajout d'utilisateur type auteur d'article
* Possibilité de d'ajouter/modifier/supprimer des catégories depuis la vue "Réglages"
* Possibilité d'afficher tous les articles d'un même auteur
* Affichage/Gestion des commentaires